<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_detail_Meteo extends CI_Model {
    
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    public function select_nom_plage($prmid)
    {
        $query = $this->db->select('nom')
                          ->from('plage')
                          ->where('plage.IDplage', $prmid)
                          ->get();
        return $query->result_array();
    }

    public function select_temp_eau($prmid)
    {
        $query = $this->db->select('IDplage, temperature')
                          ->from('meseau, plage')
                          ->where('plage.IDplage', $prmid)
                          ->get();
        return $query->result_array();
    }
    public function select_detail_by_uv($prmid)
    {
        $query = $this->db->select('*')
                          ->from('mesuv')
                          ->where('IDuv', $prmid)
                          ->get();
        return $query->result_array();
    }

    public function insert_indexUV($prmDataUv)
    {
        $this->db->insert('mesuv', $prmDataUv) ;
        $last_id = $this->db->insert_id() ;
        return $this->select_detail_by_uv($last_id) ; 
    }
}