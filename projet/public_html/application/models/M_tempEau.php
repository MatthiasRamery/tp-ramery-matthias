<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_tempEau extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function insert_tempEau($prmArrayData)
    {
        $this->db->insert('meseau', $prmArrayData);
        $last_id = $this->db->insert_id();
        return $this->select_detail_by_mesure($last_id);
    }

    public function select_tempEau()
    {
        $query = $this->db->select('*')
            ->from('plage')
            ->get();
        return $query->result_array();
    }

    public function select_detail_by_mesure($prmid)
    {
        $query = $this->db->select('*')
            ->from('meseau')
            ->where('IDeau', $prmid)
            ->get();
        return $query->result_array();
    }
}
