<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php' ;

class C_tempEau extends REST_Controller {
      
	public  function __construct() {           
 		parent::__construct();
           
		$this->load->model('M_tempEau');        
	}

	public function index_post(){
		
		$temp = $_POST['temp'];				
		              		
		$arrayTempeau = [

 			"temperature" => $temp,                     
			
			"IDplage" => 2
  		];

		$var = $this->M_tempEau->insert_tempEau($arrayTempeau) ;
	}
}