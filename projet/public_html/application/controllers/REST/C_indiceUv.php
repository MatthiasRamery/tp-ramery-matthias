<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php' ;

class C_indiceUv extends REST_Controller {
        
        public  function __construct() {
            parent::__construct();
            $this->load->model('M_indiceUv');
            

        }
	public function index_get()   
	{
                $results = $this->M_indiceUv->select_all_uv() ;
                $this->response($results, REST_Controller::HTTP_OK) ;  
	}
        
        public function index_post()   
        {                
                $dto = $this->_post_args['dto'] ;
                $dto = (array) json_decode($dto) ;
                $results = $this->M_indiceUv->insert_indexUV($dto) ;
                $this->response($results[0], REST_Controller::HTTP_CREATED) ;
        }
        

}

