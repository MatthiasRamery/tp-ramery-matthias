$(document).ready(function () {

    $('.burger').click(function () {
	    $(this).toggleClass('burger-nav-open');
        $('.navigation').toggleClass('is-open');
    });

    $('.navigation ul li a').click(function (event) {
        $('.navigation').toggleClass('is-open');
        $('.burger').toggleClass('burger-nav-open');
        event.stopPropagation();
    });

    $("#cookies-show").click(function (e) {

        e.preventDefault();
        setCookies();

    });

    $('#HidePassword').hide() ; 


    $("#Password").keyup(function(){

        resetInput() ;
        $('#HidePassword').show() ;

    });

    $("#Email").keyup(function(){

        resetInput() ;

    });

    $('#HidePassword').click(function() {

        if($("#Password").prop('type') == 'password') {
            
            $("#Password").prop('type','text');
            
            $(this).removeClass('fa-eye');
            $(this).addClass('fa-eye-slash');

        } else {
            
            $("#Password").prop('type','password');
            
            $(this).removeClass('fa-eye-slash');
            $(this).addClass('fa-eye');

        }
    });

});

function setCookies() 
{

    $.ajax({
        type: "POST",
        url: "http://hydrasconnect.com/index.php/cookiesSettings/",
        dataType: "json",
        success: function (reponse, status) {

            if (reponse.StatusCookies) 
            {
                
                $(".cookie-alert").show();

            } else {

                $(".cookie-alert").remove();

            }
        }
    });

}

function resetInput() 
{
        $("#Email").removeClass('alert-error');
        $("#Password").removeClass('alert-error');
        $(".error-icon").hide();
        $('.alert-login').remove();
}