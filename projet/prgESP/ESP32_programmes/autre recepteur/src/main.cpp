#include <SPI.h>         //pour la liaison entre LoRa et l'ESP
#include <LoRa.h>        //pour LoRa     
#include <Arduino.h>       //pour coder en Arduino
#include "WiFi.h"       //pour se connecter au WIFI
#include <HTTPClient.h>      //pour requêtes REST

#define uS_TO_S_FACTOR 1000000  //Conversion factor for micro seconds to seconds 
#define TIME_TO_SLEEP  1       //Time ESP32 will go to sleep (in seconds)
#define URLSERVER "http://localhost/surveillancebaignade/index.php/REST/C_tempEau"
#define ssid "your ssid"
#define password "your password"
#define scl 5   //horloge
#define miso 19 //Master Output, slave Input
#define mosi 27 //Master Input, slave Output
#define ss 18   //GPIO 18 => sélectionne la pin connectées au GPIO 18
#define rst 14  //GPIO 14 => éteint les pin non utilisées
#define dio0 26 //GPIO 26 => utilisé pour une fréquence et un SF

//-------------------------------------VARIABLES---------------------------------------------//

String temp;
const char *login = ssid;
const char *motdepasse = password;

//----------------------------------------FONCTION SETUP--------------------------------------//

void setup()
{

  Serial.begin(115200);       //initialise l'esp
  WiFi.begin(login, motdepasse); //initialise le mot de passe de connexion du serveur et le user

  Serial.println("Reception LoRa"); //affichage en mode console
  SPI.begin(scl, miso, mosi, ss);   //initialise les pin SPI pour LoRa
  LoRa.setPins(ss, rst, dio0);

  if (!LoRa.begin(868E6))
  { //si la fréquence est différente de 868MHz

    Serial.println("Echec"); //affichage en mode console
    esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
    esp_deep_sleep_start();
  }

  while (WiFi.status() != WL_CONNECTED)         //tant que l'esp essaye de se connecter au wifi
  { 

    delay(1000);
    Serial.println("Connexion au WiFi...");

  }

  Serial.println("Connecté au WiFi");

}

//-------------------------------------FONCTION LOOP-----------------------------------------//

void loop(){
  int packetSize = LoRa.parsePacket();        //variable contenant le paquet reçu
  if (packetSize){          //si un paquet est reçu
    int rsender = LoRa.read();      //lit l'id du transmetteur 

    if (rsender == 10){        //si l'id du transmetteur est 10
      Serial.println("Réception du LoRa n° " + String(rsender));     //affichage console
      Serial.print("Temperature recue : ");
      temp = "temp=";      //pour que le serveur web reconnaisse la variable 'temp'

      while (LoRa.available()){    //tant que LoRa peut recevoir     
        temp += (char)LoRa.read();       //lire la température dans la trame LoRa
      }

      Serial.print(temp); //affichage température
      Serial.println(" °C");

      if (WiFi.status() == WL_CONNECTED){       //si connecté au WIFI
        HTTPClient http;
        http.begin(URLSERVER); //adresse de connexion

        http.addHeader("Content-Type", "application/x-www-form-urlencoded");       //envoi au format json
        int httpResponseCode = http.POST(temp);      //pour envoyer la température avec une requête POST vers le serveur web

        if (httpResponseCode > 0){
          String response = http.getString(); //obtenir la réponse de la requête

          Serial.println(httpResponseCode);           //retourne la requête envoyée
          Serial.println(response);            //envoi la réponse et le résultat de la requête
          Serial.println("En attente d'une autre réception");
        }
        else
        {
          Serial.print("Erreur d'envoi : ");
          Serial.println(httpResponseCode); 
        }
        http.end();        //met fin à la connexion
      }
      else
      {
        Serial.println("Erreur de connexion");
      }
    }
  }
}