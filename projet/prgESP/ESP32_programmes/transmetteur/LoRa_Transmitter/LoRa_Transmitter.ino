#include <SPI.h>
#include <Wire.h>
#include <LoRa.h>
#include <Arduino.h>

#define SENDER 10
#define uS_TO_S_FACTOR 1000000  //Conversion factor for micro seconds to seconds 
#define TIME_TO_SLEEP  10        //Time ESP32 will go to sleep (in seconds)
#define TIME_TO_SLEEP_IF_ERROR  1
#define scl 5      //horloge
#define miso 19    //Master Output, slave Input
#define mosi 27    //Master Input, slave Output
#define ss 18      //GPIO 18 => sélectionne la pin connectées au GPIO 18
#define rst 14     //GPIO 14 => éteint les pin non utilisées 
#define dio0 26    //GPIO 26 => utilisé pour une fréquence et un SF

void getTempTMP(){
  unsigned char firstByte,    //octets des registres du TMP102, nécessaires pour stocker la température en temps réel
                secByte;
  int val;    //variable qui stockera les 2 octets
  float convertTemp;   //variable pour la température convertie en °C
                 
  Wire.requestFrom(0x48, 2);      //fait une requête vers l'adresse du capteur du nombre d'octets nécessaires
  //lit les températures et les placer dans les 2 octets
  firstByte = Wire.read();
  secByte = Wire.read();
  
  val = firstByte; 

  if((firstByte & 0x80) > 0){    //si la valeur de la température est négative
    val |= 0x0F00;    //forçage à 1 sur le 1er octet
  }

  val = ((val) << 4);   //décalage de 4 sur la gauche car des bits de données ne sont pas nécessaires
  val |= (secByte >> 4);    //décalage de 4 sur la droite car des bits de données ne sont pas nécessaires

  convertTemp = val * 0.0625;    //pour convertir en °C
  printf ("%.1f", convertTemp);

  Serial.println("Envoi du paquet depuis le LoRa n°10 vers le n°20: ");          //affichage en mode console
  Serial.print(convertTemp);
  Serial.println("°C");
  
  LoRa.beginPacket();    //début de la trame à envoyer
  LoRa.write(SENDER);
  LoRa.print(convertTemp, 1);
  LoRa.endPacket();    //fin trame et envoi de la trame LoRa 
}

void setup() {
  Wire.begin(21, 22);     //initialise l'adresse I2C su capteur
  Serial.begin(115200);     //initialise le débit en bauds (bits/s)
  Serial.println("Transmetteur LoRa");    //affichage en mode console
   
  SPI.begin(scl, miso, mosi, ss);     //initialisation des broches SPI pour LoRa
  LoRa.setPins(ss, rst, dio0);   

  if (!LoRa.begin(868E6)) {    //si la bande de fréquence rçue est différente de 868MHz
    Serial.println("Echec");
    esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP_IF_ERROR * uS_TO_S_FACTOR);     //temps pendant lequel l'esp va s'endormir
    esp_deep_sleep_start();    //entrée en mode sommeil profond
  }
}

void loop() {
 getTempTMP();
 esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);     //temps pendant lequel l'esp va s'endormir
  Serial.println("L'ESP32 va s'endormir pendant " + String(TIME_TO_SLEEP / 60) + " minutes");
  esp_deep_sleep_start();    //entrée en mode sommeil profond
}
